import tensorflow as tf
from tensorflow.keras.layers import Activation, Input, Conv1D, Dense
from tensorflow.keras import regularizers
from tensorflow.keras.optimizers import Adam
from sklearn.model_selection import train_test_split
from tensorflow.keras.models import Model
from tensorflow.keras import backend as K
import numpy as np
from pylab import *
tf.config.threading.set_intra_op_parallelism_threads(1)
tf.config.threading.set_inter_op_parallelism_threads(1)


class Baseline:
    def __init__(self,inputs,outputs, spectral_dim,test_size=0.5):
        self.spectral_dim = spectral_dim
        self.model = self.baseline_generator()
        self.inputs = inputs
        self.outputs = outputs
        self.test_size = test_size
        self.input_train,self.input_test,self.output_train,self.output_test = self.split_training()
        
    def baseline_generator(self):
        inp_layer = Input(shape=(1,self.spectral_dim,),name="ml_input")
        
        x = tf.keras.layers.Conv1D(256,1, activation='sigmoid')(inp_layer)
        x = tf.keras.layers.Dense(128, activation='sigmoid')(x)
        x = tf.keras.layers.Dense(64, activation='sigmoid')(x)
        x = tf.keras.layers.Dense(32, activation='sigmoid')(x)
        x = tf.keras.layers.Dense(1, activation='sigmoid')(x)

        return tf.keras.Model(inputs=[inp_layer], outputs=[x], name='Baseline')


    def split_training(self):
        inputs_train,inputs_test,outputs_train,outputs_test = train_test_split(self.inputs,self.outputs,test_size=self.test_size)
        return inputs_train,inputs_test,outputs_train,outputs_test
    
    def train(self,learning_rate=0.001, epochs=500,val_split=0.2,batch_size=128):
        adam_opt = Adam(learning_rate=learning_rate)
        x,y = self.input_train.shape
        a,b = self.output_train.shape
        self.model.compile(optimizer=adam_opt,loss=['mse'],metrics=['mae','mape'])
        self.history = self.model.fit(self.input_train.reshape(x,1,y),self.output_train.reshape(a,1,b), epochs=epochs, validation_split=val_split,batch_size=batch_size)

    def plot_loss(self,fname):
        fig, axes = plt.subplots()
        axes.plot(self.history.history['loss'],color='b',label='Training')
        axes.plot(self.history.history['val_loss'],color='r',label='Validation')
        axes.legend(loc='upper center',ncol=2)
        axes.set_ylabel('Loss')
        axes.set_xlabel('Epochs')
        plt.savefig(fname)
        plt.clf()
        plt.close()

    
    def predict(self):
        return self.model.predict(self.input_test)
        
    def save(self, path):
        self.model.save(path)
