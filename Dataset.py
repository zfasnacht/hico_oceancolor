import h5py
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler

class Dataset(object):

        def __init__(self, fname,n_chan):
                self.fname = fname
                self.dataset = self.load_dataset()
                self.n_chan = n_chan
                self.pc = self.decompose()
                self.inputs, self.inp_scalar = self.prep_inputs()
                self.outputs, self.output_scalar = self.prep_outputs()
            
        def load_dataset(self):
                dataset = {}
                f = h5py.File(self.fname,'r')
                for key in f.keys():
                        dataset[key] = f[key][()]
                f.close()
                return dataset
        def decompose(self):
            
                log_rad = np.log(self.dataset['Radiance'])
                covmatrix = np.dot(log_rad.T ,log_rad)
                _, pca_coeff = np.linalg.eigh(covmatrix)
                return pca_coeff[:,::-1]
        def prep_inputs(self):
                pca_proj = np.swapaxes(np.dot(self.pc.T,np.log(self.dataset['Radiance']).T),0,1)
                sc_in = MinMaxScaler(feature_range=(0.0,1))
                inputs = sc_in.fit_transform(pca_proj[:,0:self.n_chan])
                return inputs, sc_in
        def prep_outputs(self):
                sc_out = MinMaxScaler(feature_range=(0.0,1))
                outputs = sc_out.fit_transform(np.log(self.dataset['MODISChlorophyll']).reshape(-1,1))
                return outputs, sc_out

        def save_scalar(self,fname):
                f = h5py.File(fname,'w')
                f.create_dataset('PCA_Coeff',data=self.pc)
                f.create_dataset('Input_Scale',data=self.inp_scalar.scale_)
                f.create_dataset('Input_Min',data=self.inp_scalar.data_min_)
                f.create_dataset('Output_Scale',data=self.output_scalar.scale_)
                f.create_dataset('Output_Min',data=self.output_scalar.data_min_)
                f.close()

        
            


            
