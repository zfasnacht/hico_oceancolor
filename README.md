# HICO_OceanColor


## Dependencies
- TensorFlow>=2.3
- scikit-learn
- numpy
- h5py 

## Pre-processing 

The current set up uses measured radiances from the HICO instrument at the native resolution from 440nm to 800nm. In the first step of pre-propecessing the radiances are decomposed the Log of the radiances into principal components in the function decompose. The coefficients of the principal components are then scaled to 0->1 in the function prep_input to be used as input to the NN. The chlorophyll are then also scaled to 0->1 in the function prep_outputs. 

## Training

There model used for testing is a simple 5 layer NN with an initial CONV1D layer and four DENSE layers. At the moment they all use sigmoid activation. 

The training data can be found on the sharepoint AIST-21 project under HICO_OceanColor_ExampleData


To train the model, run: `python3 train.py -c config.json`

`config.json` contains hyperparameters, e.g.
```
{
    "dataset"         : "Test",
    "fname"           : "HICO_TrainingData.h5",
    "learning_rate"   : 0.0001,
    "epochs"          : 500,
    "test_size"       : 0.2,
    "NumberOfPCs"     : 10 ,
}
```


Once the model has been trained, a directory `<repo root>/trained_models` will be created. This directory will contain:
- Saved h5 file containing the graph of the model and the weights

