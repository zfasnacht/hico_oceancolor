from Dataset import Dataset
from models import Baseline
import os
import json
import argparse

def train(config):

    train_data = Dataset(config['fname'],config['NumberOfPCs'])
    train_data.save_scalar('HICO_Scaling.h5')
    output_dir = './trained_models/'
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)


    model_wrapper = Baseline(train_data.inputs,train_data.outputs,config['NumberOfPCs'],test_size=config['test_size'])
    model_wrapper.train(learning_rate=config['learning_rate'],epochs=config['epochs'])
    model_wrapper.save('trained_models')
    model_wrapper.plot_loss('HICO_NN_Loss.png')
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='OceanColor')
    parser.add_argument('-c', '--config',
        type=str,
	default='./config.json',
	help='path to config file')
    args = parser.parse_args()
    with open(args.config) as json_data_file:
        config = json.load(json_data_file)
    train(config)
        
